package com.alexKuzyaev.sensorsManager.Storage;

import com.alexKuzyaev.sensorsManager.Enums.UserType;
import org.mongodb.morphia.annotations.Entity;

@Entity("SM_USER")
public class SM_USER extends BaseEntity {

    public String houseId;
    public String name;
    public UserType type;

}