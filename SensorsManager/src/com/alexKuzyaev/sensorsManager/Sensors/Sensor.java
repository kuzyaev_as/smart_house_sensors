package com.alexKuzyaev.sensorsManager.Sensors;

public interface Sensor {
    String sensorId = "0";
    Boolean active = false;
    SensorType type = SensorType.ANALOG;

    void start();
    void stop();
}