package com.alexKuzyaev.sensorsManager;

import com.alexKuzyaev.sensorsManager.Transport.MongoDbTransport;

public class Main {

    public static void main(String[] args) {
	// write your code here
        MongoDbTransport.setupDatastore();
        SensorsManager.setTestingScenario();
    }
}