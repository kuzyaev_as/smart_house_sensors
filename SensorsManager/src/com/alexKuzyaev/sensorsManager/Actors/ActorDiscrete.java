package com.alexKuzyaev.sensorsManager.Actors;

import com.alexKuzyaev.sensorsManager.Sensors.SensorDiscrete;

public class ActorDiscrete extends ActorBase {

    public SensorDiscrete sensor;

    public ActorDiscrete() {
        actorType = ActorType.DISCRETE;
    }

    @Override
    public void setValue(Integer value) {
        if (value == 0) {
            this.value = 0;
        } else if (value == 1) {
            this.value = 1;
        }
    }
}