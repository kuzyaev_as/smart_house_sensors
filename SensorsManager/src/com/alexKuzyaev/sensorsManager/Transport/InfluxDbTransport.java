package com.alexKuzyaev.sensorsManager.Transport;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class InfluxDbTransport {

//    Еще раз новые настройки подключения:
//    инфлюкс: influx-taberum-461f.aivencloud.com: 26215
//    логин: avnadmin
//    пароль: eabvua78m3ko6nyo
//
//    графана:   grafana-taberum-461f.aivencloud.com
//    логин: avnadmin
//    пароль: j2osyza843sdz83c

    public static final String DB_NAME = "defaultdb";
    public static final String SERVER_ENDPOINT = "https://influx-taberum-461f.aivencloud.com:26215";

    public static void writeToInfluxData(String measurement, String field, Integer value){
        InfluxDB influxDB = InfluxDBFactory.connect(SERVER_ENDPOINT, "avnadmin", "eabvua78m3ko6nyo");

        Date date = new Date();
        long longDate = date.getTime();

        influxDB.write(DB_NAME, "autogen", Point.measurement(measurement)
                .time(longDate, TimeUnit.MILLISECONDS)
                .addField(field, value)
                .build());
        influxDB.close();
    }
}
